# Certificates deployement script

For more information, see https://gnugeneration.epfl.ch/membres/tlscertificates

## Quick install:

```
adduser --disabled-password --home /var/lib/certificates certificates
sudo -u certificates -s
cd /var/lib/certificates
ssh-keygen -t rsa -b 4096
# Ajouter la clé publique comme deployment key dans GitLab
git clone 'ssh://git@gitlab-ssh.gnugen.ch:2200/gnugen-admin/certs-deploy.git' .
git checkout gnugen
git branch --set-upstream-to=origin/gnugen
git init --bare git
ln -sf ../../bin/post-update-hook git/hooks/post-update
```

Then add the following to the `sudoers` file:

    # Needed for certificates deployment
    certificates ALL=(root) NOPASSWD:/var/lib/certificates/bin/install.sh

And finally don't forget to check that login from rapperswil with the
certificates user are allowed in `/etc/security/access.conf`.

```
# Allow login with 'certificates' user from rapperswil
+ : certificates : rapperswil.gnugen.ch
```

## Host-specific configuration

Host-specific configuration is in shell-fragment files with the name of the
host where they will be run in the `conf/` directory.
