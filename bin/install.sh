#!/bin/sh

check_chain ()
{
	x="$(openssl x509 -in "$1" -noout -text | grep Issuer: | awk '{ print $NF }')"
	case "$x" in
	R3)
		openssl verify -CApath /etc/ssl/certs -CAfile "workdir/lets-encrypt-r3.pem" "$1" >/dev/null
		;;
	R10)
		openssl verify -CApath /etc/ssl/certs -CAfile "workdir/lets-encrypt-r10.pem" "$1" >/dev/null
		;;
	R11)
		openssl verify -CApath /etc/ssl/certs -CAfile "workdir/lets-encrypt-r11.pem" "$1" >/dev/null
		;;
	X3)
		openssl verify -CApath /etc/ssl/certs -CAfile "workdir/lets-encrypt-x3-cross-signed.pem" "$1" >/dev/null
		;;
	X1)
		openssl verify -CApath /etc/ssl/certs -CAfile "workdir/lets-encrypt-x1-cross-signed.pem" "$1" >/dev/null
		;;
	*)
		echo "Unknown certificate issuer" 1>&2
		return 1
	esac

	# shellcheck disable=SC2181
	[ "$?" != "0" ] && echo  "  unable to verify certificate chain" && return 1
	return 0
}

cat_letsencrypt_chain ()
{
	x="$(openssl x509 -in "$1" -noout -text | grep Issuer: | awk '{ print $NF }')"
	case "$x" in
	R3)
		#CHECKME:
		cat "$@" "workdir/lets-encrypt-r3.pem" "workdir/lets-encrypt-x1-cross-signed.pem" "workdir/isrgrootx1.pem"
		;;
	R10)
		cat "$@" "workdir/lets-encrypt-r10.pem" "workdir/lets-encrypt-x1-cross-signed.pem" "workdir/isrgrootx1.pem"
		;;
	R11)
		cat "$@" "workdir/lets-encrypt-r11.pem" "workdir/lets-encrypt-x1-cross-signed.pem" "workdir/isrgrootx1.pem"
		;;
	X3)
		cat "$@" "workdir/lets-encrypt-x3-cross-signed.pem" "workdir/dstrootcax3.pem"
		;;
	X1)
		cat "$@" "workdir/lets-encrypt-x1-cross-signed.pem" "workdir/isrgrootx1.pem"
		;;
	*)
		echo "Unknown certificate issuer" 1>&2
		return 1
	esac
}

warn_not_installed ()
{
	echo "THE NEW CERTIFICATE HAS NOT BEEN INSTALLED!" >&2
	return 1
}

install_nginx ()
{
	echo "  $1 for nginx"
	if [ ! -e "/etc/nginx/certs" ]
	then
		mkdir -p /etc/nginx/certs
		chown root:root /etc/nginx/certs
		chmod 750 /etc/nginx/certs
	fi

	cert="workdir/certificates/$1.crt"
	check_chain "$cert" || warn_not_installed || return 1
	cat_letsencrypt_chain "$cert" \
		| install --mode=440 --owner=root --group=root \
			"/dev/stdin" \
			"/etc/nginx/certs/$1.pem"
	install --mode=440 --owner=root --group=root \
		"workdir/keys/$1.key" \
		"/etc/nginx/certs/$1.key"

	RESTART_NGINX=1
}

install_dovecot ()
{
	echo "  $1 for dovecot"
	if [ ! -e "/etc/dovecot/private" ]
	then
		mkdir -p /etc/dovecot/private
		chmod 700 /etc/dovecot/private
	fi

	cert="workdir/certificates/$1.crt"
	check_chain "$cert" || warn_not_installed || return 1
	install --mode=400 --owner=root --group=root \
		"workdir/keys/$1.key" \
		"/etc/dovecot/private/ssl_key.pem"
	cat_letsencrypt_chain "$cert" \
		| install --mode=440 --owner=root --group=dovecot \
			"/dev/stdin" \
			"/etc/dovecot/ssl_cert.pem"

	RESTART_DOVECOT=1
}

install_ldap ()
{
	echo "  $1 for slapd"
	if [ ! -e "/etc/ldap/certs/" ]
	then
		mkdir -p "/etc/ldap/certs"
		chmod 750 "/etc/ldap/certs"
		chown root:openldap "/etc/ldap/certs"
	fi

	cert="workdir/certificates/$1.crt"
	check_chain "$cert" || warn_not_installed || return 1
	install --mode=440 --owner=root --group=openldap \
		"workdir/keys/$1.key" \
		"/etc/ldap/certs/$1.key"
	install --mode=440 --owner=root --group=openldap \
		"workdir/certificates/$1.crt" \
		"/etc/ldap/certs/$1.pem"
	cat_letsencrypt_chain "$cert" \
		| install --mode=440 --owner=root --group=openldap \
			"/dev/stdin" \
			"/etc/ldap/certs/$1-ca.pem"

	RESTART_LDAP=1
}

install_exim ()
{
	echo "  $1 for exim"
	if [ ! -e "/etc/exim4/certs/" ]
	then
		mkdir -p "/etc/exim4/certs"
		chmod 750 "/etc/exim4/certs"
		chown root:Debian-exim
	fi

	cert="workdir/certificates/$1.crt"
	check_chain "$cert" || warn_not_installed || return 1
	install --mode=640 --owner=root --group=Debian-exim \
		"workdir/keys/$1.key" \
		"/etc/exim4/certs/$1.key"
	cat_letsencrypt_chain "$cert" \
		| install --mode=640 --owner=root --group=Debian-exim \
			"/dev/stdin" \
			"/etc/exim4/certs/$1.pem"

	RESTART_EXIM=1
}

install_radicale ()
{
	echo "  $1 for radicale"
	if [ ! -e "/etc/radicale/certs" ]
	then
		mkdir -p "/etc/radicale/certs"
		chown root:radicale "/etc/radicale/certs"
		chmod 750 "/etc/radicale/certs"
	fi

	cert="workdir/certificates/$1.crt"
	check_chain "$cert" || warn_not_installed || return 1
	install --mode=440 --owner=root --group=radicale \
		"workdir/keys/$1.key" \
		"/etc/radicale/certs/$1.key"
	cat_letsencrypt_chain "$cert" \
		| install --mode=440 --owner=root --group=radicale \
			"/dev/stdin" \
			"/etc/radicale/certs/$1.pem"

	RESTART_RADICALE=1
}

install_znc ()
{
	echo "  $1 for znc"
	if [ ! -e "/etc/znc/" ]
	then
		mkdir -p "/etc/znc"
		chown root:znc "/etc/znc"
		chmod 750 "/etc/znc"
	fi

	cert="workdir/certificates/$1.crt"
	key="workdir/keys/$1.key"
	check_chain "$cert" || warn_not_installed || return 1
	printf "%s\n%s" "$(cat "$key")" "$(cat_letsencrypt_chain "$cert")" \
		| install --mode=440 --owner=root --group=znc \
		"/dev/stdin" \
		"/etc/znc/znc.pem"

	RESTART_ZNC=1
}

install_pureftpd ()
{
	echo "  $1 for Pure-FTPd"
	cert="workdir/certificates/$1.crt"
	key="workdir/keys/$1.key"
	check_chain "$cert" || warn_not_installed || return 1
	printf "%s\n%s" "$(cat "$key")" "$(cat_letsencrypt_chain "$cert")" \
		| install --mode=440 --owner=root --group=root \
		"/dev/stdin" \
		"/etc/ssl/private/pure-ftpd.pem"

	RESTART_PUREFTPD=1
}

install_conode ()
{
	echo "  $1 for conode"
	cert="workdir/certificates/$1.crt"
	key="workdir/keys/$1.key"
	check_chain "$cert" || warn_not_installed || return 1
	install --mode=440 --owner=conode --group=nogroup \
		"workdir/keys/$1.key" \
		"/var/lib/conode/certs/conode.key"
	cat_letsencrypt_chain "$cert" \
		| install --mode=440 --owner=conode --group=nogroup \
			"/dev/stdin" \
			"/var/lib/conode/certs/conode.pem"

	RESTART_CONODE=1
}

check_restart_nginx ()
{
	if [ "$RESTART_NGINX" = "1" ]
	then
		systemctl restart nginx.service
	fi
}

check_restart_dovecot ()
{
	if [ "$RESTART_DOVECOT" = "1" ]
	then
		systemctl restart dovecot.service
	fi
}

check_restart_ldap ()
{
	if [ "$RESTART_LDAP" = "1" ]
	then
		systemctl restart slapd.service
	fi
}

check_restart_exim ()
{
	if [ "$RESTART_EXIM" = "1" ]
	then
		systemctl restart exim4.service
	fi
}

check_restart_radicale ()
{
	if [ "$RESTART_RADICALE" = "1" ]
	then
		systemctl restart radicale.service
	fi
}

check_restart_znc ()
{
	if [ "$RESTART_ZNC" = "1" ]
	then
		systemctl restart znc.service
	fi
}

check_restart_pureftpd ()
{
	if [ "$RESTART_PUREFTPD" = "1" ]
	then
		systemctl restart pure-ftpd.service
	fi
}

check_restart_conode ()
{
	if [ "$RESTART_CONODE" = "1" ]
	then
		systemctl restart conode.service
	fi
}


# Used to install certificates in systemd containers ('machines').
run_in_machine ()
{
	machine=$1
	install_command=$2
	machine_basedir=/root/certificates
	artifacts=/var/lib/machines/$machine/root/certificates

	if [ -z "$machine" ]; then
		echo "run_in_machine() requires a machine name!"
		exit 1
	fi

	# Remove any possibly existing artifacts
	echo "Remove artefacts on machine $machine ..."
	# machinectl shell $machine /bin/rm -rf $machine_basedir
	# ^ the above fails for some files, applying ugly workaround for now ...
	if [ -d "$artifacts" ]; then
		rm -rf "$artifacts"
	fi
	echo "Uploading certificates and deploy script to $machine..."
	machinectl shell "$machine" /bin/mkdir -p "$machine_basedir"

	# Copy installation script and certificates to the machine's root directory
	# Note: machinectl copy-to chown the destination file to UID/GID 0 but let
	# the mod bits untouched.
	machinectl copy-to "$machine" bin/install.sh "$machine_basedir/install.sh"
	machinectl copy-to "$machine" workdir "$machine_basedir/workdir"

	# Run installation script in machine.
	echo "Executing installation script on $machine..."
	machinectl shell --setenv=BASEDIR="$machine_basedir" --setenv=COMMAND="$install_command" "$machine" "$machine_basedir/install.sh"

	# Remove artifacts
	# machinectl shell $machine /bin/rm -rf $machine_basedir
	# ^ the above fails for some files, applying ugly workaround for now ...
	echo "Remove artefacts on machine $machine ..."
	if [ -d "$artifacts" ]; then
		rm -rf "$artifacts"
	fi
}

if [ ! "$(whoami)" = "root" ]
then
	echo "$0: This script must be run as root."
	exit 1
fi

# Set this script's 'base' directory, not to be confused with the 'workdir'
# contained the certificates.
if [ -z "$BASEDIR" ]; then
	BASEDIR="/var/lib/certificates"
fi
cd $BASEDIR || warn "Could not cd to $BASEDIR" || exit 1

# If no command is defined, run script stored under conf/$(hostname) of the
# base directory.
HOST="$(hostname)"
if [ -z "$COMMAND" ]; then
	SCRIPT="$BASEDIR/conf/$HOST"

	echo "Installing certificates on $HOST using script '$SCRIPT' ..."
	. "$SCRIPT"
	echo "Done."
else
	echo "Installing certificates on $HOST using command '$COMMAND' ..."
	$COMMAND
	echo "Done."
fi

check_restart_nginx
check_restart_dovecot
check_restart_ldap
check_restart_exim
check_restart_radicale
check_restart_znc
check_restart_pureftpd
