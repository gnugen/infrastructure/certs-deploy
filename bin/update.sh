#!/bin/sh

die ()
{
	printf "ERROR: %s\n" "$@" >&2
	exit 1
}

cd "/var/lib/certificates"
echo "Updating deployment scripts..."

if [ ! "$(whoami)" = "certificates" ]
then
	die "This script must be run as the certificates user."
fi

if [ -n "$(git status --porcelain)" ]
then
	die "Aborting update since '$PWD' is unclean."
fi

git fetch \
	|| die "Failed to fetch remote state."
git merge --ff-only \
	|| die "Failed to fast-forward to the upstream branch ($(git for-each-ref --format='%(upstream:short)' "$(git symbolic-ref -q HEAD)"))."
